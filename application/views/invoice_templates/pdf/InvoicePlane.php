<!DOCTYPE html>
<html lang="en">
<title><?php _trans('invoice');?></title>
<body style="margin: 0;">

<div class="table-content" style="">
    <?php if ($invoice->extended): ?>
        <table style="margin-left:55px; margin-top:51px; width:100%">
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td></td>
                <td width="125px" height="100px" class="velmie-icon">
                    <img style="width: 140px; height: 34px" src="<?php echo FCPATH . 'application/views/invoice_templates/pdf/velmie.png'; ?>"/>
                </td>
            </tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td colspan="5"></td>
                <td class="text-right raleway-bold-first-page" colspan="4"><?php echo $invoice->invoice_title; ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td class="text-right raleway-regular-first-page" colspan="4"><?php echo $invoice->invoice_description; ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td class="text-right raleway-regular-first-page" colspan="4"><?php echo date("F d, Y"); ?> by Velmie, LLC</td>
                <td></td>
            </tr>
        </table>
    <?php endif; ?>
    <?php if ($invoice->extended): ?>
        <pagebreak>
        <table style="width:100%">
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td colspan="8">
                    <div class="block-title-wrapper">
                        <span class="raleway-regular-title">Project Progress</span>
                        <hr class="line"/>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr><td colspan="10"></td></tr>
            <tr style="font-size: 40px">
                <td></td>
                <td colspan="1" style="letter-spacing: 2px;">
                    <img style="width: 160px; height: 160px" src="<?php echo create_diagram($invoice); ?>">
                </td>
                <td colspan="7">
                    <div class="gothampro-regular-black"><?php echo formatted_hours($invoice->hours_completed); ?></div>
                    <div class="gothampro-regular-grey">completed from the total</div>
                    <div class="gothampro-regular-grey">estimate</div>
                    <div>
                        <table><tr><td></td></tr></table>
                    </div>
                    <div class="gothampro-regular-black"><?php echo formatted_hours($invoice->hours_total - $invoice->hours_completed); ?></div>
                    <div class="gothampro-regular-grey">of work remaining</div>
                </td>
                <td></td>
            </tr>
        </table>
    <?php endif; ?>
    <?php if ($invoice->extended): ?>
        <!--<pagebreak>-->
        <table style="width:100%">
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td></td>
                <td colspan="8">
                    <div class="block-title-wrapper">
                        <span class="raleway-regular-title">Project Credentials</span>
                        <hr class="line"/>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td></td>
                <td class="gothampro-bold" colspan="8">Sandbox URL:</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="8">
                    <a class="fs-16 gothampro-regular-letter-1" href="<?php echo $invoice->sandbox_url; ?>"><?php echo $invoice->sandbox_url; ?></a>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="8" class="gothampro-regular-grey">
                    <table class="insert-table">
                        <tr>
                            <td class="gothampro-regular-grey" style="padding-left: 0">Login:</td><td></td><td></td><td class="gothampro-regular-black"><?php echo $invoice->sandbox_login; ?></td>
                        </tr>
                        <tr>
                            <td style="padding-left: 0"></td><td></td><td></td><td></td>
                        </tr>
                        <tr>
                            <td class="gothampro-regular-grey" style="padding-left: 0">Password:</td><td></td><td></td><td class="gothampro-regular-black"><?php echo $invoice->sandbox_password; ?></td>
                        </tr>
                    </table>

                </td>
                <td></td>
            </tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td></td>
                <td class="gothampro-bold" colspan="8">Jira URL:</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="8">
                    <a class="fs-16 gothampro-regular-letter-1" href="<?php echo $invoice->jira_url; ?>"><?php echo $invoice->jira_url; ?></a>
                </td>
                <td></td>
            </tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td></td>
                <td class="gothampro-bold" colspan="8">Slack Address:</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="8">
                    <a class="fs-16 gothampro-regular-letter-1" href="<?php echo $invoice->slack_address; ?>"><?php echo $invoice->slack_address; ?></a>
                </td>
                <td></td>
            </tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td></td>
                <td class="gothampro-bold" colspan="8">Bitbucket URL:</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="8">
                    <a class="fs-16 gothampro-regular-letter-1" href="<?php echo $invoice->bitbucket_url; ?>"><?php echo $invoice->bitbucket_url; ?></a>
                </td>
                <td></td>
            </tr>
        </table>
    <?php endif; ?>
    <?php if ($invoice->extended): ?>
        <pagebreak>
        <table style="width:100%">
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td></td>
                <td colspan="8">
                    <div class="block-title-wrapper">
                        <span class="raleway-regular-title">Periodic Invoice</span>
                        <hr class="line"/>
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
    <?php endif; ?>
    <?php $this->layout->load_view('invoice_templates/pdf/InvoicePlane_part'); ?>
    <!--///////////////////////////////////DOC//////////////////////////////////////-->
    <?php if ($invoice->extended): ?>
        <pagebreak>
        <table style="width:100%">
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td colspan="8" style="">
                    <div class="block-title-wrapper">
                        <span class="raleway-regular-title">Meetings Schedule</span>
                        <hr class="line"/>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr><td colspan="10"></td></tr>
            <tr style="font-size: 40px">
                <td></td>
                <td class="gothampro-regular-black" style="width: 400px; line-height: 2" colspan="8">
                    You are welcome to join daily standup meeting and sprint plannings to stay aligned with the project status and talk to the team directly.
                </td>
                <td></td>
            </tr>
            <tr><td colspan="10"></td></tr>
            <?php foreach ($invoice_meetings as $invoice_meeting): ?>
                <tr class="fs-16">
                    <td></td>
                    <td colspan="8" class="gothampro-bold">
                        <?php echo $invoice_meeting->meeting_week_day; ?>
                    </td>
                    <td></td>
                </tr>
                <tr class="fs-16">
                    <td></td>
                    <td colspan="8" class="gothampro-regular-grey">
                        <?php echo $invoice_meeting->meeting_time; ?>
                    </td>
                    <td></td>
                </tr>
                <tr class="fs-16">
                    <td></td>
                    <td colspan="8" class="gothampro-regular-grey">
                        <?php echo $invoice_meeting->meeting_description; ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="8">
                        <a class="fs-16 gothampro-regular-letter-1" href="<?php echo $invoice_meeting->meeting_link; ?>">Join</a>
                    </td>
                    <td></td>
                </tr>
                <tr><td colspan="10"></td></tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    <?php if ($invoice->extended): ?>
        <pagebreak>
        <table style="width:100%">
            <tr><td colspan="10"></td></tr>
            <tr><td colspan="10"></td></tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td colspan="8">
                    <div class="block-title-wrapper">
                        <span class="raleway-regular-title">Team Contacts</span>
                        <hr class="line"/>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr><td colspan="10"></td></tr>
            <?php foreach ($invoice_contacts as $invoice_contact): ?>
                <tr>
                    <td></td>
                    <td colspan="8" >
                        <table style="width: 100%">
                            <tr>
                                <td class="contact">
                                    <table><tr><td style="padding: 10px 10px"></td></tr></table>
                                    <table class="no-padding" style="width: 100%">
                                        <tr>
                                            <td colspan="9">
                                                <div class="gothampro-bold"><?php echo $invoice_contact->contact_full_name; ?></div>
                                            </td>
                                            <td class="text-right" rowspan="3">
                                                <?php if ($invoice_contact->contact_photo && file_exists(FCPATH . $invoice_contact->contact_photo)): ?>
                                                    <img style="width: 72px; height: 72px" src="<?php echo create_circle(FCPATH . $invoice_contact->contact_photo); ?>">
                                                <?php else: ?>
                                                    <img style="width: 72px; height: 72px" src="<?php echo create_circle(FCPATH . 'assets/core/img/no-image.jpg'); ?>">
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9">
                                                <div class="fs-12 gothampro-regular-letter-1"><?php echo $invoice_contact->contact_position; ?></div>
                                            </td>
                                        </tr>
                                        <tr><td colspan="9" style="height: 20px"></td></tr>
                                    </table>
                                    <div>
                                        <span>
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="14px" height="14px" viewBox="0 0 438.536 438.536"
                                                 xml:space="preserve">
                                                <path d="M415.7,262.093c3.039-14.657,4.564-28.935,4.564-42.826c0-27.218-5.276-53.247-15.844-78.087
                                                    c-10.562-24.838-24.838-46.249-42.825-64.237C343.608,58.955,322.2,44.68,297.361,34.114
                                                    c-24.845-10.559-50.866-15.843-78.088-15.843c-13.894,0-28.171,1.524-42.827,4.57C156.651,7.614,134.381,0,109.637,0
                                                    C79.375,0,53.538,10.705,32.124,32.115c-21.416,21.416-32.12,47.253-32.12,77.516c0,24.744,7.614,47.014,22.839,66.809
                                                    c-3.044,14.655-4.568,28.933-4.568,42.827c0,27.215,5.28,53.243,15.843,78.085c10.562,24.838,24.838,46.25,42.827,64.241
                                                    c17.987,17.986,39.401,32.257,64.239,42.824c24.84,10.564,50.869,15.845,78.087,15.845c13.893,0,28.17-1.526,42.834-4.572
                                                    c19.79,15.229,42.058,22.847,66.804,22.847c30.259,0,56.103-10.711,77.505-32.12c21.416-21.416,32.12-47.253,32.12-77.519
                                                    C438.529,304.158,430.918,281.891,415.7,262.093z M325.472,304.49c-6.276,11.136-14.702,20.033-25.263,26.696
                                                    c-10.567,6.663-22.224,11.748-34.975,15.273c-12.751,3.518-26.073,5.283-39.971,5.283c-32.163,0-59.855-6.235-83.078-18.705
                                                    c-23.223-12.471-34.833-27.453-34.833-44.968c0-8.562,2.428-15.693,7.282-21.408c4.853-5.712,12.038-8.562,21.555-8.562
                                                    c5.52,0,10.657,1.522,15.415,4.564c4.758,3.046,9.135,6.715,13.134,10.999c3.999,4.285,8.326,8.562,12.99,12.847
                                                    c4.661,4.285,10.847,7.946,18.555,10.992c7.71,3.046,16.418,4.572,26.128,4.572c12.371,0,22.36-2.423,29.981-7.275
                                                    c7.61-4.859,11.416-10.999,11.416-18.418c0-7.618-3.042-13.326-9.13-17.132c-4.182-2.673-14.846-6.098-31.977-10.283
                                                    l-41.688-10.284c-11.419-2.662-21.222-5.752-29.408-9.271c-8.186-3.524-15.8-8.134-22.841-13.849
                                                    c-7.039-5.708-12.369-12.891-15.986-21.555c-3.616-8.658-5.424-18.796-5.424-30.406c0-13.896,3.189-26.121,9.563-36.688
                                                    c6.374-10.565,14.849-18.846,25.409-24.841c10.562-5.996,21.935-10.468,34.119-13.418c12.179-2.951,24.742-4.426,37.685-4.426
                                                    c18.276,0,35.589,2.19,51.961,6.567c16.368,4.377,29.882,10.801,40.538,19.271c10.657,8.473,15.985,17.942,15.985,28.409
                                                    c0,8.562-2.707,15.893-8.138,21.982c-5.414,6.088-12.601,9.134-21.55,9.134c-4.948,0-9.514-1.143-13.702-3.427
                                                    c-4.186-2.283-7.99-5.042-11.423-8.278c-3.426-3.234-7.047-6.423-10.852-9.563c-3.806-3.14-8.946-5.852-15.41-8.136
                                                    c-6.479-2.284-13.802-3.427-21.986-3.427c-27.406,0-41.112,7.323-41.112,21.982c0,3.234,0.715,6.09,2.141,8.564
                                                    c1.427,2.474,3,4.473,4.71,5.996c1.709,1.525,4.565,3.046,8.564,4.57c3.999,1.525,7.33,2.622,9.994,3.284
                                                    c2.663,0.666,6.854,1.665,12.562,2.996l29.694,6.851c9.325,2.096,17.648,4.286,24.981,6.567c7.331,2.288,15.181,5.521,23.555,9.71
                                                    c8.374,4.187,15.321,8.848,20.838,13.988c5.523,5.144,10.137,11.656,13.853,19.555c3.714,7.901,5.564,16.61,5.564,26.124
                                                    C334.896,280.84,331.761,293.357,325.472,304.49z"/>
                                            </svg>
                                        </span>
                                        &nbsp;
                                        <span class="fs-12 mb-5 gothampro-regular-letter-1"><?php echo $invoice_contact->contact_skype; ?></span>
                                    </div>
                                    <table><tr><td style="padding: 4px 10px"></td></tr></table>
                                    <div>
                                        <span>
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="14px" height="14px" viewBox="0 0 14 14" xml:space="preserve">
                                                    <path style="fill:#030104;" d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986
                                                        c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/>
                                                    <path style="fill:#030104;" d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8
                                                        L13.684,2.271z"/>
                                                    <polygon style="fill:#030104;" points="0,2.878 0,11.186 4.833,7.079 		"/>
                                                    <polygon style="fill:#030104;" points="9.167,7.079 14,11.186 14,2.875 		"/>
                                            </svg>
                                        </span>
                                        &nbsp;
                                        <span class="fs-12 mb-5 gothampro-regular-letter-1"><?php echo $invoice_contact->contact_email; ?></span>
                                    </div>
                                    <table><tr><td style="padding: 4px 10px"></td></tr></table>
                                    <div>
                                        <span>
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="14px" height="14px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
                                                <path d="M25.302,0H9.698c-1.3,0-2.364,1.063-2.364,2.364v30.271C7.334,33.936,8.398,35,9.698,35h15.604
                                                    c1.3,0,2.364-1.062,2.364-2.364V2.364C27.666,1.063,26.602,0,25.302,0z M15.004,1.704h4.992c0.158,0,0.286,0.128,0.286,0.287
                                                    c0,0.158-0.128,0.286-0.286,0.286h-4.992c-0.158,0-0.286-0.128-0.286-0.286C14.718,1.832,14.846,1.704,15.004,1.704z M17.5,33.818
                                                    c-0.653,0-1.182-0.529-1.182-1.183s0.529-1.182,1.182-1.182s1.182,0.528,1.182,1.182S18.153,33.818,17.5,33.818z M26.021,30.625
                                                    H8.979V3.749h17.042V30.625z"/>
                                            </svg>
                                        </span>
                                        &nbsp;
                                        <span class="fs-12 gothampro-regular-letter-1"><?php echo $invoice_contact->contact_phone; ?></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td></td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>

<style>
    .line {
        color: #f5f5f5;
        margin: 1px 0 0;
    }
    @page {
        margin-top: 0.05cm;
        margin-bottom: 0.05cm;
        margin-left: 0.2cm;
        margin-right: 0.05cm;
    }
    <?php if ($invoice->extended): ?>
        @page :first {
            background: #00ffef;
        }
    <?php endif; ?>
    .raleway-bold-first-page {
        border-bottom: 1px solid #00ffd3;
        font-family: 'RalewayBold', sans-serif;
        font-size: 32px;
        letter-spacing: 1px;
        padding: 5px 0
    }
    .raleway-regular-first-page {
        border-bottom: 1px solid #00ffd3;
        font-family: 'RalewayRegular', sans-serif;
        font-size: 16px;
        letter-spacing: 1px;
        padding: 5px 0
    }
    .raleway-regular-title {
        text-transform: uppercase;
        border-bottom: 1px solid #00ffd3;
        font-family: 'RalewayRegular', sans-serif;
        font-size: 16px;
        letter-spacing: 3px;
        padding: 5px;
        line-height: 5;
    }
    .gothampro-regular-black {
        color: #000000;
        font-family: 'GothamProRegular', sans-serif;
        font-size: 16px;
        letter-spacing: 1px;
        line-height: 1;
    }
    .gothampro-regular-grey {
        color: #878484;
        font-family: 'GothamProRegular', sans-serif;
        font-size: 16px;
        letter-spacing: 1px;
        line-height: 1;
    }
    .gothampro-bold {
        font-family: 'GothamProBold', sans-serif;
        font-size: 16px;
        letter-spacing: 1px;
        line-height: 1;
    }
    .gothampro-regular-letter-1 {
        font-family: 'GothamProRegular', sans-serif;
        letter-spacing: 1px;
    }
    .block-title-wrapper {
        /*border-bottom: 1px solid #f5f5f5;*/
        font-size: 16px;
        padding: 5px;
        width: 100%;
    }
    .text-right {
        text-align: right;
    }
    .fs-12 {
        font-size: 12px;
    }
    .fs-16 {
        font-size: 16px;
    }
	.mb-5 {
		margin-bottom: 5px;
	}
    .velmie-icon img {
        width: 50%;
        height: 50%;
    }

    .table-content td.contact {
        border: 1px solid #f5f5f5;
        padding: 10px 30px;
		position: relative;
        line-height: 2.17;
    }
    .table-content {
        max-width: 800px;
    }
    .table-content table {
        border-spacing: 0 0;
        border-collapse: collapse;
        font-size: 12px;
        color: black;
    }
    .table-content td {
        min-width: 30px;
        padding: 10px;
    }
	.table-content .no-padding td {
        padding: 0;
    }
    .table-content tr {
        padding: 0 10px;
    }
    .border-bottom-td td {
        border-bottom: 1px solid #000;
    }

    .table-content-part table {
        border-spacing: 0 0;
        border-collapse: collapse;
        font-family: gothampro;
        font-size: 12px;
        color: black;
    }
    .border-bottom {
        border-bottom: 1px solid #ccc;
    }
    .background-blue {
        background: blue;
        color: white;
    }
    .bold-text {
        font-weight: bold;
    }
    .f11 {
        font-size: 11px;
    }
    .grey {
        color: grey;
    }
    .table-content .insert-table td {
        padding: 5px;
    }
</style>
</body>
</html>
