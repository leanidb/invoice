<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Mdl_Meetings
 */
class Mdl_Meetings extends Response_Model
{
    public $table = 'ip_invoice_meetings';

    public $primary_key = 'ip_invoice_meetings.meeting_id';

    public function default_select()
    {
        $this->db->select('ip_invoice_meetings.*');
    }

    public function default_order_by()
    {
        $this->db->order_by('ip_invoice_meetings.meeting_order');
    }

    /**
     * @param null $meeting_id
     * @param null $db_array
     *
     * @return int|null
     */
    public function save($meeting_id = null, $db_array = null)
    {
        $meeting_id = parent::save($meeting_id, $db_array);

        return $meeting_id;
    }

    /**
     * @param int $meeting_id
     *
     * @return bool
     */
    public function delete($meeting_id)
    {
        parent::delete($meeting_id);

        return true;
    }
}
