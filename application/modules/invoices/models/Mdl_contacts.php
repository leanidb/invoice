<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Mdl_Contacts
 */
class Mdl_Contacts extends Response_Model
{
    public $table = 'ip_contacts';

    public $primary_key = 'ip_contacts.contact_id';

    public function default_select()
    {
        $this->db->select('ip_contacts.*');
    }

    /**
     * @param null $contact_id
     * @param null $db_array
     *
     * @return int|null
     */
    public function save($contact_id = null, $db_array = null)
    {
        $contact_id = parent::save($contact_id, $db_array);

        return $contact_id;
    }
}
