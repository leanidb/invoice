<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Mdl_Invoice_contacts
 */
class Mdl_Invoice_contacts extends Response_Model
{
    public $table = 'ip_invoice_contacts';

    public $primary_key = 'ip_invoice_contacts.id';

    public function default_select()
    {
        $this->db->select('ip_invoice_contacts.*');
    }

    public function default_order_by()
    {
        $this->db->order_by('ip_invoice_contacts.contact_order');
    }

    public function get_join_table()
    {
        return $this->db->from('ip_invoice_contacts')->join('ip_contacts', 'ip_contacts.contact_id = ip_invoice_contacts.contact_id');
    }

    /**
     * @param null $invoice_contact_id
     * @param null $db_array
     *
     * @return int|null
     */
    public function save($invoice_contact_id = null, $db_array = null)
    {
        $invoice_contact_id = parent::save($invoice_contact_id, $db_array);

        return $invoice_contact_id;
    }

    /**
     * @param int $invoice_contact_id
     *
     * @return bool
     */
    public function delete($invoice_contact_id)
    {
        parent::delete($invoice_contact_id);

        return true;
    }
}
