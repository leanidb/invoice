<div class="table-responsive">
    <table id="contact_table" class="items table table-condensed table-bordered no-margin">
        <tbody id="new_contact" style="display: none;">
        <tr>
            <td rowspan="2" class="td-icon">
                <i class="fa fa-arrows cursor-move"></i>
                <input type="hidden" name="invoice_id" value="<?php echo $invoice_id; ?>">
                <input type="hidden" id="contact_id" name="contact_id" value="">
                <input type="hidden" name="invoice_contact_id" value="">
            </td>
            <td class="td-amount">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('email'); ?></span>
                    <select id="contact_email" name="contact_email" class="form-control input-sm">
                        <option value=""><?php _trans('none'); ?></option>
                        <?php foreach ($exlContacts as $exlContact) { ?>
                            <option value="<?php echo $exlContact->contact_email; ?>">
                                <?php echo $exlContact->contact_email; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </td>
            <td class="td-text">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('new_email'); ?></span>
                    <input type="text" id="contact_new_email" name="contact_new_email" class="input-sm form-control" value="">
                </div>
            </td>
            <td class="td-text">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('full_name'); ?></span>
                    <input type="text" id="contact_full_name" name="contact_full_name" class="input-sm form-control" value="">
                </div>
            </td>
            <td class="td-icon text-right td-vert-middle">
                <button type="button" class="btn_delete_contact btn btn-link btn-sm" title="<?php _trans('delete'); ?>">
                    <i class="fa fa-trash-o text-danger"></i>
                </button>
            </td>
        </tr>
        <tr>
            <td class="td-text">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('position'); ?></span>
                    <input type="text" id="contact_position" name="contact_position" class="input-sm form-control" value="">
                </div>
            </td>
            <td class="td-text">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('skype'); ?></span>
                    <input type="text" id="contact_skype" name="contact_skype" class="input-sm form-control" value="">
                </div>
            </td>
            <td class="td-text">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('phone'); ?></span>
                    <input type="text" id="contact_phone" name="contact_phone" class="input-sm form-control" value="">
                </div>
            </td>
            <td class="td-text">
                <!--
                <div class="input-group">
                    <span class="input-group-addon"><?php //_trans('photo'); ?></span>
                    <input type="text" id="contact_photo" name="contact_photo" class="input-sm form-control" value="">
                </div>
                -->
                <?php $this->layout->load_view('upload/dropzone-contact-html', ['contactId' => '']); ?>
            </td>
        </tr>
        </tbody>

        <?php if ($invoiceContacts) { ?>
            <?php foreach ($invoiceContacts as $invoiceContact) { ?>
                <tbody class="contact">
                <tr>
                    <td rowspan="2" class="td-icon">
                        <i class="fa fa-arrows cursor-move"></i>
                        <input type="hidden" name="invoice_id" value="<?php echo $invoice_id; ?>">
                        <input type="hidden" name="contact_id" value="<?php echo $invoiceContact->contact_id; ?>">
                        <input type="hidden" name="invoice_contact_id" value="<?php echo $invoiceContact->id; ?>">
                    </td>
                    <td class="td-amount">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('email'); ?></span>
                            <select id="contact_email" name="contact_email" class="form-control input-sm" disabled="disabled">
                                <?php foreach ($contacts as $contact) { ?>
                                    <option value="<?php echo $contact->contact_email; ?>"
                                        <?php check_select($invoiceContact->contact_email, $contact->contact_email); ?>>
                                        <?php echo  $contact->contact_email; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </td>
                    <td class="td-text">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('new_email'); ?></span>
                            <input type="text" id="contact_new_email" name="contact_new_email" class="input-sm form-control" value="" disabled="disabled">
                        </div>
                    </td>
                    <td class="td-text">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('full_name'); ?></span>
                            <input type="text" id="contact_full_name" name="contact_full_name" class="input-sm form-control"
                                   value="<?php _htmlsc($invoiceContact->contact_full_name); ?>"
                                <?php if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                } ?>>
                        </div>
                    </td>
                    <td class="td-icon text-right td-vert-middle">
                        <?php if ($invoice->is_read_only != 1): ?>
                            <button type="button" class="btn_delete_contact btn btn-link btn-sm" title="<?php _trans('delete'); ?>"
                                    data-invoice-contact-id="<?php echo $invoiceContact->id; ?>">
                                <i class="fa fa-trash-o text-danger"></i>
                            </button>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="td-text">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('position'); ?></span>
                            <input type="text" id="contact_position" name="contact_position" class="input-sm form-control"
                                   value="<?php _htmlsc($invoiceContact->contact_position); ?>"
                                <?php if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                } ?>>
                        </div>
                    </td>
                    <td class="td-text">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('skype'); ?></span>
                            <input type="text" id="contact_skype" name="contact_skype" class="input-sm form-control"
                                   value="<?php _htmlsc($invoiceContact->contact_skype); ?>"
                                <?php if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                } ?>>
                        </div>
                    </td>
                    <td class="td-text">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('phone'); ?></span>
                            <input type="text" id="contact_phone" name="contact_phone" class="input-sm form-control"
                                   value="<?php _htmlsc($invoiceContact->contact_phone); ?>"
                                <?php if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                } ?>>
                        </div>
                    </td>
                    <td class="td-text">
                        <!--
                        <div class="input-group">
                            <span class="input-group-addon"><?php //_trans('photo'); ?></span>
                            <input type="text" id="contact_photo" name="contact_photo" class="input-sm form-control"
                                   value="<?php //_htmlsc($invoiceContact->contact_photo); ?>"
                                <?php /*if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                }*/ ?>>
                        </div>
                        -->
                        <?php $this->layout->load_view('upload/dropzone-contact-html', ['contactId' => $invoiceContact->contact_id]); ?>
                    </td>
                </tr>
                </tbody>
            <?php } ?>
        <?php } ?>

    </table>
</div>

<br>

<div class="row">
    <div class="col-xs-12 col-md-4">
        <div class="btn-group">
            <?php if ($invoice->is_read_only != 1) { ?>
                <a href="#" class="btn_add_contact btn btn-sm btn-default">
                    <i class="fa fa-plus"></i> <?php _trans('add_new_contact'); ?>
                </a>
            <?php } ?>
        </div>
    </div>

    <div class="col-xs-12 visible-xs visible-sm"><br></div>

    <div class="col-xs-12 col-md-6 col-md-offset-2 col-lg-4 col-lg-offset-4">
    </div>
</div>
