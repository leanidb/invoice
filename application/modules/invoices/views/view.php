<?php
$cv = $this->controller->view_data["custom_values"];
?>

<script>
    $(function () {
        $(document).find('#extended').is(':checked') ? $('#extended_block').show() : $('#extended_block').hide();

        $('#extended').click(function() {
            this.checked ? $('#extended_block').show(1000) : $('#extended_block').hide(1000); //time for show
        });

        $('.item-task-id').each(function () {
            // Disable client chaning if at least one item already has a task id assigned
            if ($(this).val().length > 0) {
                $('#invoice_change_client').hide();
                return false;
            }
        });

        $('.btn_add_product').click(function () {
            $('#modal-placeholder').load(
                "<?php echo site_url('products/ajax/modal_product_lookups'); ?>/" + Math.floor(Math.random() * 1000)
            );
        });

        $('.btn_add_task').click(function () {
            $('#modal-placeholder').load(
                "<?php echo site_url('tasks/ajax/modal_task_lookups/' . $invoice_id); ?>/" +
                Math.floor(Math.random() * 1000)
            );
        });

        $('.btn_add_row').click(function () {
            $('#new_row').clone().appendTo('#item_table').removeAttr('id').addClass('item').show();
        });

        $('.btn_add_meeting').click(function () {
            $('#new_meeting').clone().appendTo('#meeting_table').removeAttr('id').addClass('meeting').show();
        });

        $('.btn_add_contact').click(function () {
            var excl_contact_emails = [];
            $('.contact #contact_email').each(function () {
                excl_contact_emails[excl_contact_emails.length] = $(this).val();
                $(this).attr('disabled','disabled');
            });
            $('#new_contact #contact_email option').each(function() {
                if($.inArray($(this).val(), excl_contact_emails) !== -1 && $(this).val() !== ''){
                    //console.log($(this).val());
                    $(this).hide();
                }
            });

            $('#new_contact').clone().appendTo('#contact_table').removeAttr('id').addClass('contact').show();

            previewNodes = document.querySelectorAll('.contact #template_photo');
            dropZones = document.querySelectorAll('.contact .drop_zone');
            previewPhotos = document.querySelectorAll('.contact #previews_photo');
            fileinputButtonPhotos = document.querySelectorAll('.contact .fileinput-button-photo');

            previewNodes[previewNodes.length - 1].id = '';
            previewTemplates[previewTemplates.length] = previewNodes[previewNodes.length - 1].parentNode.innerHTML;
            previewNodes[previewNodes.length - 1].parentNode.removeChild(previewNodes[previewNodes.length - 1]);

            myDropzones[myDropzones.length] = new Dropzone(dropZones[dropZones.length - 1], { // Make the whole body a dropzone
                url: '<?php echo site_url('upload/upload_photo') ?>',
                thumbnailWidth: 80,
                thumbnailHeight: 80,
                parallelUploads: 20,
                uploadMultiple: false,
                dictRemoveFileConfirmation: '<?php _trans('delete_photo_warning'); ?>',
                previewTemplate: previewTemplates[previewTemplates.length - 1],
                autoQueue: true, // Make sure the files aren't queued until manually added
                previewsContainer: previewPhotos[previewPhotos.length - 1], // Define the container to display the previews
                clickable: fileinputButtonPhotos[fileinputButtonPhotos.length - 1], // Define the element that should be used as click trigger to select files.
            });

            myDropzones[myDropzones.length - 1].on('success', function (file, response) {
                <?php echo(IP_DEBUG ? 'console.log(response);' : ''); ?>
                if (typeof response !== 'undefined') {
                    response = JSON.parse(response);
                    if (response.success === true) {
                        this.element.querySelector('#contact_photo').setAttribute('value', response.contact_photo);
                        this.clickableElements[0].setAttribute('disabled','disabled');
                    }
                    if (response.success !== true) {
                        alert(response.message);
                    }
                }
            });

            myDropzones[myDropzones.length - 1].on('removedfile', function (file) {
                var contactPhoto = this.element.firstElementChild.getAttribute('value');
                this.element.firstElementChild.setAttribute('value', '');
                this.clickableElements[0].removeAttribute('disabled');

                $.post({
                    url: '<?php echo site_url('upload/delete_photo') ?>',
                    data: {
                        'contact_photo': contactPhoto
                    }
                });
            });

        });

        $(document).on('change', '#contact_email', function () {
            var select = $(this);
            var parentNode;

            $.post("<?php echo site_url('invoices/ajax/get_contact'); ?>", {
                    contact_email: $(select).val()
                },
                function (data) {
                    parentNode = $(select).parent();
                    do {
                        parentNode = parentNode.parent();
                    } while (parentNode[0].tagName != 'TBODY');

                    var response = JSON.parse(data);
                    parentNode.find('#contact_id').val(response.id);
                    parentNode.find('#contact_full_name').val(response.full_name);
                    parentNode.find('#contact_full_name').val(response.full_name);
                    parentNode.find('#contact_position').val(response.position);
                    parentNode.find('#contact_skype').val(response.skype);
                    parentNode.find('#contact_phone').val(response.phone);
                    parentNode.find('#contact_photo').val(response.photo);
                    parentNode.find('#temporary_contact_id').val(response.id);

                    var thisDropzone = myDropzones[myDropzones.length - 1];
                    if (thisDropzone.element.querySelector('.dz-success')) {
                        thisDropzone.element.querySelector('.dz-success').remove();
                    }
                    if (response.success === 1) {
                        parentNode.find('#contact_new_email').val('');
                        parentNode.find('#contact_new_email').attr('disabled','disabled');

                        var contactId = thisDropzone.element.firstElementChild.nextElementSibling.getAttribute('value');
                        $.ajax({
                            url: '<?php echo site_url('invoices/ajax/get_contact_photo'); ?>' + '?contact_id=' + contactId,
                            dataType: 'json',
                            type: 'get',
                            contentType: 'application/json',
                            async: false,
                            success: function(data) {
                                if (data.contact_photo && data.success) {
                                    var mockFile = {};
                                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, '<?php echo site_url(); ?>' + data.contact_photo);
                                    thisDropzone.emit('success', mockFile);

                                    thisDropzone.element.firstElementChild.setAttribute('value', data.contact_photo);
                                    thisDropzone.clickableElements[0].setAttribute('disabled', 'disabled');
                                } else {
                                    parentNode.find('.fileinput-button-photo').removeAttr('disabled');
                                }
                            }
                        });

                    } else {
                        parentNode.find('#contact_new_email').removeAttr('disabled');
                        parentNode.find('.fileinput-button-photo').removeAttr('disabled');
                    }
                });
        });

    <?php if (!$items) { ?>
        $('#new_row').clone().appendTo('#item_table').removeAttr('id').addClass('item').show();
        <?php } ?>

        $('#btn_create_recurring').click(function () {
            $('#modal-placeholder').load(
                "<?php echo site_url('invoices/ajax/modal_create_recurring'); ?>",
                {
                    invoice_id: <?php echo $invoice_id; ?>
                }
            );
        });

        $('#invoice_change_client').click(function () {
            $('#modal-placeholder').load("<?php echo site_url('invoices/ajax/modal_change_client'); ?>", {
                invoice_id: <?php echo $invoice_id; ?>,
                client_id: "<?php echo $this->db->escape_str($invoice->client_id); ?>",
            });
        });

        $('#btn_save_invoice').click(function () {
            var items = [];
            var item_order = 1;
            $('table tbody.item').each(function () {
                var row = {};
                $(this).find('input,select,textarea').each(function () {
                    if ($(this).is(':checkbox')) {
                        row[$(this).attr('name')] = $(this).is(':checked');
                    } else {
                        row[$(this).attr('name')] = $(this).val();
                    }
                });
                row['item_order'] = item_order;
                item_order++;
                items.push(row);
            });
            var meetings = [];
            var meeting_order = 1;
            $('table tbody.meeting').each(function () {
                var row = {};
                $(this).find('input,select,textarea').each(function () {
                    if ($(this).is(':checkbox')) {
                        row[$(this).attr('name')] = $(this).is(':checked');
                    } else {
                        row[$(this).attr('name')] = $(this).val();
                    }
                });
                row['meeting_order'] = meeting_order;
                meeting_order++;
                meetings.push(row);
            });
            var contacts = [];
            var contact_order = 1;
            $('table tbody.contact').each(function () {
                var row = {};
                $(this).find('input,select,textarea').each(function () {
                    if ($(this).is(':checkbox')) {
                        row[$(this).attr('name')] = $(this).is(':checked');
                    } else {
                        row[$(this).attr('name')] = $(this).val();
                    }
                });
                row['contact_order'] = contact_order;
                contact_order++;
                contacts.push(row);
            });
            $.post("<?php echo site_url('invoices/ajax/save'); ?>", {
                    invoice_id: <?php echo $invoice_id; ?>,
                    invoice_number: $('#invoice_number').val(),
                    invoice_date_created: $('#invoice_date_created').val(),
                    invoice_date_due: $('#invoice_date_due').val(),
                    invoice_status_id: $('#invoice_status_id').val(),
                    invoice_password: $('#invoice_password').val(),
                    items: JSON.stringify(items),
                    invoice_discount_amount: $('#invoice_discount_amount').val(),
                    invoice_discount_percent: $('#invoice_discount_percent').val(),
                    invoice_terms: $('#invoice_terms').val(),
                    custom: $('input[name^=custom],select[name^=custom]').serializeArray(),
                    payment_method: $('#payment_method').val(),
                    invoice_title: $('#invoice_title').val(),
                    invoice_description: $('#invoice_description').val(),
                    hours_completed: $('#hours_completed').val(),
                    sandbox_url: $('#sandbox_url').val(),
                    sandbox_login: $('#sandbox_login').val(),
                    sandbox_password: $('#sandbox_password').val(),
                    jira_url: $('#jira_url').val(),
                    slack_address: $('#slack_address').val(),
                    bitbucket_url: $('#bitbucket_url').val(),
                    meetings: JSON.stringify(meetings),
                    contacts: JSON.stringify(contacts),
                    extended: $('#extended').is(':checked') ? 1 : 0,
                    period: $('#period').val()
                },
                function (data) {
                    <?php echo(IP_DEBUG ? 'console.log(data);' : ''); ?>
                    var response = JSON.parse(data);
                    if (response.success === 1) {
                        window.location = "<?php echo site_url('invoices/view'); ?>/" + <?php echo $invoice_id; ?>;
                    } else {
                        $('#fullpage-loader').hide();
                        $('.control-group').removeClass('has-error');
                        $('div.alert[class*="alert-"]').remove();
                        var resp_errors = response.validation_errors,
                            all_resp_errors = '';
                        for (var key in resp_errors) {
                            $('#' + key).parent().addClass('has-error');
                            all_resp_errors += resp_errors[key];
                        }
                        $('#invoice_form').prepend('<div class="alert alert-danger">' + all_resp_errors + '</div>');
                    }
                });
        });

        $('#btn_generate_pdf').click(function () {
            window.open('<?php echo site_url('invoices/generate_pdf/' . $invoice_id); ?>', '_blank');
        });

        $(document).on('click', '.btn_delete_item', function () {
            var btn = $(this);
            var item_id = btn.data('item-id');

            // Just remove the row if no item ID is set (new row)
            if (typeof item_id === 'undefined') {
                $(this).parents('.item').remove();
            }

            $.post("<?php echo site_url('invoices/ajax/delete_item/' . $invoice->invoice_id); ?>", {
                    'item_id': item_id,
                },
                function (data) {
                    <?php echo(IP_DEBUG ? 'console.log(data);' : ''); ?>
                    var response = JSON.parse(data);

                    if (response.success === 1) {
                        btn.parents('.item').remove();
                    } else {
                        btn.removeClass('btn-link').addClass('btn-danger').prop('disabled', true);
                    }
                });
        });

        $(document).on('click', '.btn_delete_meeting', function () {
            var btn = $(this);
            var meeting_id = btn.data('meeting-id');

            // Just remove the row if no meeting ID is set (new row)
            if (typeof meeting_id === 'undefined') {
                $(this).parents('.meeting').remove();
            }

            $.post("<?php echo site_url('invoices/ajax/delete_meeting'); ?>", {
                    'invoice_id': <?php echo $invoice->invoice_id; ?>,
                    'meeting_id': meeting_id
                },
                function (data) {
                    <?php echo(IP_DEBUG ? 'console.log(data);' : ''); ?>
                    var response = JSON.parse(data);

                    if (response.success === 1) {
                        btn.parents('.meeting').remove();
                    } else {
                        btn.removeClass('btn-link').addClass('btn-danger').prop('disabled', true);
                    }
                });
        });

        $(document).on('click', '.btn_delete_contact', function () {
            var btn = $(this);
            var invoice_contact_id = btn.data('invoice-contact-id');

            // Just remove the row if no invoice_contact ID is set (new row)
            if (typeof invoice_contact_id === 'undefined') {
                $(this).parents('.contact').remove();
            }

            $.post("<?php echo site_url('invoices/ajax/delete_invoice_contact'); ?>", {
                    'invoice_id': <?php echo $invoice->invoice_id; ?>,
                    'invoice_contact_id': invoice_contact_id
                },
                function (data) {
                    <?php echo(IP_DEBUG ? 'console.log(data);' : ''); ?>
                    var response = JSON.parse(data);

                    if (response.success === 1) {
                        btn.parents('.contact').remove();

                        $('#new_contact option').each(function () {
                            if ($(this).val()) {
                                $(this).remove();
                            }
                        });
                        $.each(response.exlContacts, function(i, item) {
                            $('#new_contact select').append($('<option>', {
                                value: item.contact_email,
                                text: item.contact_email
                            }));
                        });
                    } else {
                        btn.removeClass('btn-link').addClass('btn-danger').prop('disabled', true);
                    }
                });
        });

        <?php if ($invoice->is_read_only != 1): ?>
        var fixHelper = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        };

        $('#item_table').sortable({
            items: 'tbody',
            helper: fixHelper,
        });

        $('#meeting_table').sortable({
            items: 'tbody',
            helper: fixHelper,
        });

        $('#contact_table').sortable({
            items: 'tbody',
            helper: fixHelper,
        });

        if ($('#invoice_discount_percent').val().length > 0) {
            $('#invoice_discount_amount').prop('disabled', true);
        }

        if ($('#invoice_discount_amount').val().length > 0) {
            $('#invoice_discount_percent').prop('disabled', true);
        }

        $('#invoice_discount_amount').keyup(function () {
            if (this.value.length > 0) {
                $('#invoice_discount_percent').prop('disabled', true);
            } else {
                $('#invoice_discount_percent').prop('disabled', false);
            }
        });
        $('#invoice_discount_percent').keyup(function () {
            if (this.value.length > 0) {
                $('#invoice_discount_amount').prop('disabled', true);
            } else {
                $('#invoice_discount_amount').prop('disabled', false);
            }
        });
        <?php endif; ?>

        <?php if ($invoice->invoice_is_recurring) : ?>
        $(document).on('click', '.js-item-recurrence-toggler', function () {
            var itemRecurrenceState = $(this).next('input').val();
            if (itemRecurrenceState === ('1')) {
                $(this).next('input').val('0');
                $(this).removeClass('fa-calendar-check-o text-success');
                $(this).addClass('fa-calendar-o text-muted');
            } else {
                $(this).next('input').val('1');
                $(this).removeClass('fa-calendar-o text-muted');
                $(this).addClass('fa-calendar-check-o text-success');
            }
        });
        <?php endif; ?>

    });
</script>

<?php
echo $modal_delete_invoice;
echo $modal_add_invoice_tax;
if ($this->config->item('disable_read_only') == true) {
    $invoice->is_read_only = 0;
}
?>

<div id="headerbar">
    <h1 class="headerbar-title">
        <?php
        echo trans('invoice') . ' ';
        echo($invoice->invoice_number ? '#' . $invoice->invoice_number : $invoice->invoice_id);
        ?>
    </h1>

    <div class="headerbar-item pull-right <?php if ($invoice->is_read_only != 1 || $invoice->invoice_status_id != 4) { ?>btn-group<?php } ?>">

        <div class="options btn-group btn-group-sm">
            <a class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-caret-down no-margin"></i> <?php _trans('options'); ?>
            </a>
            <ul class="dropdown-menu">
                <?php if ($invoice->is_read_only != 1) { ?>
                    <li>
                        <a href="#add-invoice-tax" data-toggle="modal">
                            <i class="fa fa-plus fa-margin"></i> <?php _trans('add_invoice_tax'); ?>
                        </a>
                    </li>
                <?php } ?>
                <li>
                    <a href="#" id="btn_create_credit" data-invoice-id="<?php echo $invoice_id; ?>">
                        <i class="fa fa-minus fa-margin"></i> <?php _trans('create_credit_invoice'); ?>
                    </a>
                </li>
                <?php if ($invoice->invoice_balance != 0) : ?>
                    <li>
                        <a href="#" class="invoice-add-payment"
                           data-invoice-id="<?php echo $invoice_id; ?>"
                           data-invoice-balance="<?php echo $invoice->invoice_balance; ?>"
                           data-invoice-payment-method="<?php echo $invoice->payment_method; ?>">
                            <i class="fa fa-credit-card fa-margin"></i>
                            <?php _trans('enter_payment'); ?>
                        </a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="#" id="btn_generate_pdf"
                       data-invoice-id="<?php echo $invoice_id; ?>">
                        <i class="fa fa-print fa-margin"></i>
                        <?php _trans('download_pdf'); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('mailer/invoice/' . $invoice->invoice_id); ?>">
                        <i class="fa fa-send fa-margin"></i>
                        <?php _trans('send_email'); ?>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#" id="btn_create_recurring"
                       data-invoice-id="<?php echo $invoice_id; ?>">
                        <i class="fa fa-repeat fa-margin"></i>
                        <?php _trans('create_recurring'); ?>
                    </a>
                </li>
                <li>
                    <a href="#" id="btn_copy_invoice"
                       data-invoice-id="<?php echo $invoice_id; ?>">
                        <i class="fa fa-copy fa-margin"></i>
                        <?php _trans('copy_invoice'); ?>
                    </a>
                </li>
                <?php if ($invoice->invoice_status_id == 1 || ($this->config->item('enable_invoice_deletion') === true && $invoice->is_read_only != 1)) { ?>
                    <li>
                        <a href="#delete-invoice" data-toggle="modal">
                            <i class="fa fa-trash-o fa-margin"></i>
                            <?php _trans('delete'); ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <?php if ($invoice->is_read_only != 1 || $invoice->invoice_status_id != 4) { ?>
            <a href="#" class="btn btn-sm btn-success ajax-loader" id="btn_save_invoice">
                <i class="fa fa-check"></i> <?php _trans('save'); ?>
            </a>
        <?php } ?>
    </div>

    <div class="headerbar-item invoice-labels pull-right">
        <?php if ($invoice->invoice_is_recurring) { ?>
            <span class="label label-info"><?php _trans('recurring'); ?></span>
        <?php } ?>
        <?php if ($invoice->is_read_only == 1) { ?>
            <span class="label label-danger">
                <i class="fa fa-read-only"></i> <?php _trans('read_only'); ?>
            </span>
        <?php } ?>
    </div>

</div>

<div id="content">

    <?php echo $this->layout->load_view('layout/alerts'); ?>

    <div id="invoice_form">
        <div class="invoice">

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5">

                    <h3>
                        <a href="<?php echo site_url('clients/view/' . $invoice->client_id); ?>">
                            <?php _htmlsc(format_client($invoice)) ?>
                        </a>
                        <?php if ($invoice->invoice_status_id == 1 && !$invoice->creditinvoice_parent_id) { ?>
                            <span id="invoice_change_client" class="fa fa-edit cursor-pointer small"
                                  data-toggle="tooltip" data-placement="bottom"
                                  title="<?php _trans('change_client'); ?>"></span>
                        <?php } ?>
                    </h3>
                    <br>
                    <div class="client-address">
                        <?php $this->layout->load_view('clients/partial_client_address', ['client' => $invoice]); ?>
                    </div>
                    <?php if ($invoice->client_phone || $invoice->client_email) : ?>
                        <hr>
                    <?php endif; ?>
                    <?php if ($invoice->client_phone): ?>
                        <div>
                            <?php _trans('phone'); ?>:&nbsp;
                            <?php _htmlsc($invoice->client_phone); ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($invoice->client_email): ?>
                        <div>
                            <?php _trans('email'); ?>:&nbsp;
                            <?php _auto_link($invoice->client_email); ?>
                        </div>
                    <?php endif; ?>

                </div>

                <div class="col-xs-12 visible-xs"><br></div>

                <div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-1">
                    <div class="details-box panel panel-default panel-body">
                        <div class="row">

                            <?php if ($invoice->invoice_sign == -1) { ?>
                                <div class="col-xs-12">
                                    <div class="alert alert-warning small">
                                        <i class="fa fa-credit-invoice"></i>&nbsp;
                                        <?php
                                        echo trans('credit_invoice_for_invoice') . ' ';
                                        $parent_invoice_number = $this->mdl_invoices->get_parent_invoice_number($invoice->creditinvoice_parent_id);
                                        echo anchor('/invoices/view/' . $invoice->creditinvoice_parent_id, $parent_invoice_number);
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="col-xs-12 col-md-6">

                                <div class="invoice-properties">
                                    <label><?php _trans('invoice'); ?> #</label>
                                    <input type="text" id="invoice_number" class="form-control input-sm"
                                        <?php if ($invoice->invoice_number) : ?>
                                            value="<?php echo $invoice->invoice_number; ?>"
                                        <?php else : ?>
                                            placeholder="<?php _trans('not_set'); ?>"
                                        <?php endif; ?>
                                        <?php if ($invoice->is_read_only == 1) {
                                            echo 'disabled="disabled"';
                                        } ?>>
                                </div>

                                <div class="invoice-properties has-feedback">
                                    <label><?php _trans('date'); ?></label>

                                    <div class="input-group">
                                        <input name="invoice_date_created" id="invoice_date_created"
                                               class="form-control input-sm datepicker"
                                               value="<?php echo date_from_mysql($invoice->invoice_date_created); ?>"
                                            <?php if ($invoice->is_read_only == 1) {
                                                echo 'disabled="disabled"';
                                            } ?>>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar fa-fw"></i>
                                        </span>
                                    </div>
                                </div>

                                <div class="invoice-properties has-feedback">
                                    <label><?php _trans('due_date'); ?></label>

                                    <div class="input-group">
                                        <input name="invoice_date_due" id="invoice_date_due"
                                               class="form-control input-sm datepicker"
                                               value="<?php echo date_from_mysql($invoice->invoice_date_due); ?>"
                                            <?php if ($invoice->is_read_only == 1) {
                                                echo 'disabled="disabled"';
                                            } ?>>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar fa-fw"></i>
                                        </span>
                                    </div>
                                </div>

                                <div class="invoice-properties has-feedback">
                                    <label><?php _trans('period'); ?></label>

                                    <div class="input-group">
                                        <input type="text" name="period" id="period"
                                               class="form-control input-sm"
                                               value="<?php echo $invoice->period; ?>"
                                            <?php if ($invoice->is_read_only == 1) {
                                                echo 'disabled="disabled"';
                                            } ?>>

                                    </div>
                                </div>


                                <!-- Custom fields -->
                                <?php foreach ($custom_fields as $custom_field): ?>
                                    <?php if ($custom_field->custom_field_location != 1) {
                                        continue;
                                    } ?>
                                    <?php print_field($this->mdl_invoices, $custom_field, $cv); ?>
                                <?php endforeach; ?>

                            </div>

                            <div class="col-xs-12 col-md-6">

                                <div class="invoice-properties">
                                    <label>
                                        <?php _trans('status');
                                        if ($invoice->is_read_only != 1 || $invoice->invoice_status_id != 4) {
                                            echo ' <span class="small">(' . trans('can_be_changed') . ')</span>';
                                        } ?>
                                    </label>
                                    <select name="invoice_status_id" id="invoice_status_id"
                                            class="form-control input-sm simple-select"
                                        <?php if ($invoice->is_read_only == 1 && $invoice->invoice_status_id == 4) {
                                            echo 'disabled="disabled"';
                                        } ?>>
                                        <?php foreach ($invoice_statuses as $key => $status) { ?>
                                            <option value="<?php echo $key; ?>"
                                                    <?php if ($key == $invoice->invoice_status_id) { ?>selected="selected"<?php } ?>>
                                                <?php echo $status['label']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="invoice-properties">
                                    <label><?php _trans('payment_method'); ?></label>
                                    <select name="payment_method" id="payment_method"
                                            class="form-control input-sm simple-select"
                                        <?php if ($invoice->is_read_only == 1 && $invoice->invoice_status_id == 4) {
                                            echo 'disabled="disabled"';
                                        } ?>>
                                        <option value="0"><?php _trans('select_payment_method'); ?></option>
                                        <?php foreach ($payment_methods as $payment_method) { ?>
                                            <option <?php check_select($invoice->payment_method,
                                                $payment_method->payment_method_id) ?>
                                                value="<?php echo $payment_method->payment_method_id; ?>">
                                                <?php echo $payment_method->payment_method_name; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="invoice-properties">
                                    <label><?php _trans('invoice_password'); ?></label>
                                    <input type="text" id="invoice_password" class="form-control input-sm"
                                           value="<?php echo $invoice->invoice_password; ?>"
                                        <?php if ($invoice->is_read_only == 1) {
                                            echo 'disabled="disabled"';
                                        } ?>>
                                </div>
                            </div>

                            <?php if ($invoice->invoice_status_id != 1) { ?>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="invoice-guest-url"><?php _trans('guest_url'); ?></label>
                                        <div class="input-group">
                                            <input type="text" id="invoice-guest-url" readonly class="form-control"
                                                   value="<?php echo site_url('guest/view/invoice/' . $invoice->invoice_url_key) ?>">
                                            <span class="input-group-addon to-clipboard cursor-pointer"
                                                  data-clipboard-target="#invoice-guest-url">
                                                <i class="fa fa-clipboard fa-fw"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>

            </div>

            <input type="checkbox" id="extended" name="extended" <?php if ($invoice->extended): ?>checked="checked"<?php endif; ?>>
            <label for="extended"><?php _trans('extended'); ?></label>
            <div id="extended_block">
                <hr>
                <?php $this->layout->load_view('invoices/view_partial'); ?>
                <br>
                <?php $this->layout->load_view('invoices/partial_meeting_table'); ?>
                <br>
                <?php $this->layout->load_view('invoices/partial_contact_table'); ?>
                <hr>
            </div>

            <?php $this->layout->load_view('invoices/partial_item_table'); ?>

            <hr/>

            <div class="row">
                <div class="col-xs-12 col-md-6">

                    <div class="panel panel-default no-margin">
                        <div class="panel-heading">
                            <?php _trans('invoice_terms'); ?>
                        </div>
                        <div class="panel-body">
                            <textarea id="invoice_terms" name="invoice_terms" class="form-control" rows="3"
                                <?php if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                } ?>
                            ><?php _htmlsc($invoice->invoice_terms); ?></textarea>
                        </div>
                    </div>

                    <div class="col-xs-12 visible-xs visible-sm"><br></div>

                </div>
                <div class="col-xs-12 col-md-6">

                    <?php $this->layout->load_view('upload/dropzone-invoice-html'); ?>

                </div>
            </div>

            <?php if ($custom_fields): ?>
                <div class="row">
                    <div class="col-xs-12">

                        <hr>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php _trans('custom_fields'); ?>
                            </div>
                            <div class="panel-body">
                                <div class="row">

                                    <div class="col-xs-12 col-md-6">
                                        <?php $i = 0; ?>
                                        <?php foreach ($custom_fields as $custom_field): ?>
                                            <?php if ($custom_field->custom_field_location != 0) {
                                                continue;
                                            } ?>
                                            <?php $i++; ?>
                                            <?php if ($i % 2 != 0): ?>
                                                <?php print_field($this->mdl_invoices, $custom_field, $cv); ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <?php $i = 0; ?>
                                        <?php foreach ($custom_fields as $custom_field): ?>
                                            <?php if ($custom_field->custom_field_location != 0) {
                                                continue;
                                            } ?>
                                            <?php $i++; ?>
                                            <?php if ($i % 2 == 0): ?>
                                                <?php print_field($this->mdl_invoices, $custom_field, $cv); ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            <?php endif; ?>

        </div>

    </div>
</div>

<?php $this->layout->load_view('upload/dropzone-contact-scripts'); ?>
<?php $this->layout->load_view('upload/dropzone-invoice-scripts'); ?>
