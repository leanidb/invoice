<div class="table-responsive">
    <table id="meeting_table" class="items table table-condensed table-bordered no-margin">
        <tbody id="new_meeting" style="display: none;">
        <tr>
            <td rowspan="2" class="td-icon">
                <i class="fa fa-arrows cursor-move"></i>
                <input type="hidden" name="invoice_id" value="<?php echo $invoice_id; ?>">
                <input type="hidden" name="meeting_id" value="">
            </td>
            <td class="td-amount">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('week_day'); ?></span>
                    <select name="meeting_week_day" class="form-control input-sm">
                        <?php foreach ($weekDays as $weekDay) { ?>
                            <option value="<?php echo $weekDay; ?>"
                                <?php check_select('Monday', $weekDay); ?>>
                                <?php echo $weekDay; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </td>
            <td class="td-text">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('time'); ?></span>
                    <input type="text" name="meeting_time" class="input-sm form-control" value="">
                </div>
            </td>
            <td class="td-text">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('link'); ?></span>
                    <input type="text" name="meeting_link" class="input-sm form-control" value="">
                </div>
            </td>
            <td class="td-textarea">
                <div class="input-group">
                    <span class="input-group-addon"><?php _trans('description'); ?></span>
                    <textarea name="meeting_description" class="input-sm form-control"></textarea>
                </div>
            </td>
            <td class="td-icon text-right td-vert-middle">
                <button type="button" class="btn_delete_meeting btn btn-link btn-sm" title="<?php _trans('delete'); ?>">
                    <i class="fa fa-trash-o text-danger"></i>
                </button>
            </td>
        </tr>
        <tr>
        </tr>
        </tbody>

        <?php if ($meetings) { ?>
            <?php foreach ($meetings as $meeting) { ?>
                <tbody class="meeting">
                <tr>
                    <td rowspan="2" class="td-icon">
                        <i class="fa fa-arrows cursor-move"></i>
                        <input type="hidden" name="invoice_id" value="<?php echo $invoice_id; ?>">
                        <input type="hidden" name="meeting_id" value="<?php echo $meeting->meeting_id; ?>"
                    </td>
                    <td class="td-amount">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('week_day'); ?></span>
                            <select name="meeting_week_day" class="form-control input-sm"
                                <?php if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                } ?>>
                                <?php foreach ($weekDays as $weekDay) { ?>
                                    <option value="<?php echo $weekDay; ?>"
                                        <?php check_select($meeting->meeting_week_day, $weekDay); ?>>
                                        <?php echo $weekDay; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </td>
                    <td class="td-text">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('time'); ?></span>
                            <input type="text" name="meeting_time" class="input-sm form-control"
                                   value="<?php _htmlsc($meeting->meeting_time); ?>"
                                <?php if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                } ?>>
                        </div>
                    </td>
                    <td class="td-text">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('link'); ?></span>
                            <input type="text" name="meeting_link" class="input-sm form-control"
                                   value="<?php _htmlsc($meeting->meeting_link); ?>"
                                <?php if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                } ?>>
                        </div>
                    </td>
                    <td class="td-textarea">
                        <div class="input-group">
                            <span class="input-group-addon"><?php _trans('description'); ?></span>
                            <textarea name="meeting_description" class="input-sm form-control"
                                <?php if ($invoice->is_read_only == 1) {
                                    echo 'disabled="disabled"';
                                } ?>><?php echo htmlsc($meeting->meeting_description); ?></textarea>
                        </div>
                    </td>
                    <td class="td-icon text-right td-vert-middle">
                        <?php if ($invoice->is_read_only != 1): ?>
                            <button type="button" class="btn_delete_meeting btn btn-link btn-sm" title="<?php _trans('delete'); ?>"
                                    data-meeting-id="<?php echo $meeting->meeting_id; ?>">
                                <i class="fa fa-trash-o text-danger"></i>
                            </button>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                </tr>
                </tbody>
            <?php } ?>
        <?php } ?>

    </table>
</div>

<br>

<div class="row">
    <div class="col-xs-12 col-md-4">
        <div class="btn-group">
            <?php if ($invoice->is_read_only != 1) { ?>
                <a href="#" class="btn_add_meeting btn btn-sm btn-default">
                    <i class="fa fa-plus"></i> <?php _trans('add_new_meeting'); ?>
                </a>
            <?php } ?>
        </div>
    </div>

    <div class="col-xs-12 visible-xs visible-sm"><br></div>

    <div class="col-xs-12 col-md-6 col-md-offset-2 col-lg-4 col-lg-offset-4">
    </div>
</div>
