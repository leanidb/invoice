<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-5">
        <?php //if ($invoice->hours_total): ?>
            <!--<div>
                <?php //_trans('hours_total'); ?>:&nbsp;
                <?php //_htmlsc($invoice->hours_total); ?>
            </div>-->
            <div class="invoice-properties">
                <label><?php _trans('invoice_title'); ?>:</label>
                <input type="text" id="invoice_title" class="form-control input-sm"
                    <?php if ($invoice->invoice_title) : ?>
                        value="<?php echo $invoice->invoice_title; ?>"
                    <?php else : ?>
                        placeholder="<?php _trans('not_set'); ?>"
                    <?php endif; ?>>
            </div>
            <div class="invoice-properties">
                <label><?php _trans('invoice_description'); ?>:</label>
                <input type="text" id="invoice_description" class="form-control input-sm"
                    <?php if ($invoice->invoice_description) : ?>
                        value="<?php echo $invoice->invoice_description; ?>"
                    <?php else : ?>
                        placeholder="<?php _trans('not_set'); ?>"
                    <?php endif; ?>>
            </div>
            <div class="invoice-properties">
                <label><?php _trans('hours_completed'); ?>:</label>
                <input type="text" id="hours_completed" class="form-control input-sm"
                    <?php if ($invoice->hours_completed) : ?>
                        value="<?php echo $invoice->hours_completed; ?>"
                    <?php else : ?>
                        placeholder="<?php _trans('not_set'); ?>"
                    <?php endif; ?>>
            </div>
        <?php //endif; ?>
    </div>

    <div class="col-xs-12 visible-xs"><br></div>

    <div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-1">
        <div class="details-box panel panel-default panel-body">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="invoice-properties">
                        <label><?php _trans('sandbox_url'); ?>:</label>
                        <input type="text" id="sandbox_url" class="form-control input-sm"
                            <?php if ($invoice->sandbox_url) : ?>
                                value="<?php echo $invoice->sandbox_url; ?>"
                            <?php else : ?>
                                placeholder="<?php _trans('not_set'); ?>"
                            <?php endif; ?>>
                    </div>
                    <div class="invoice-properties">
                        <label><?php _trans('sandbox_login'); ?>:</label>
                        <input type="text" id="sandbox_login" class="form-control input-sm"
                            <?php if ($invoice->sandbox_login) : ?>
                                value="<?php echo $invoice->sandbox_login; ?>"
                            <?php else : ?>
                                placeholder="<?php _trans('not_set'); ?>"
                            <?php endif; ?>>
                    </div>
                    <div class="invoice-properties">
                        <label><?php _trans('sandbox_password'); ?>:</label>
                        <input type="text" id="sandbox_password" class="form-control input-sm"
                            <?php if ($invoice->sandbox_password) : ?>
                                value="<?php echo $invoice->sandbox_password; ?>"
                            <?php else : ?>
                                placeholder="<?php _trans('not_set'); ?>"
                            <?php endif; ?>>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="invoice-properties">
                        <label><?php _trans('jira_url'); ?>:</label>
                        <input type="text" id="jira_url" class="form-control input-sm"
                            <?php if ($invoice->jira_url) : ?>
                                value="<?php echo $invoice->jira_url; ?>"
                            <?php else : ?>
                                placeholder="<?php _trans('not_set'); ?>"
                            <?php endif; ?>>
                    </div>
                    <div class="invoice-properties">
                        <label><?php _trans('slack_address'); ?>:</label>
                        <input type="text" id="slack_address" class="form-control input-sm"
                            <?php if ($invoice->slack_address) : ?>
                                value="<?php echo $invoice->slack_address; ?>"
                            <?php else : ?>
                                placeholder="<?php _trans('not_set'); ?>"
                            <?php endif; ?>>
                    </div>
                    <div class="invoice-properties">
                        <label><?php _trans('bitbucket_url'); ?>:</label>
                        <input type="text" id="bitbucket_url" class="form-control input-sm"
                            <?php if ($invoice->bitbucket_url) : ?>
                                value="<?php echo $invoice->bitbucket_url; ?>"
                            <?php else : ?>
                                placeholder="<?php _trans('not_set'); ?>"
                            <?php endif; ?>>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>