<div class="panel panel-default no-margin">

    <div class="panel-body clearfix">
        <div class="input-group">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <?php //if ($contactId): ?>
                <button type="button" class="btn btn-default fileinput-button-photo form-control">
                    <i class="fa fa-plus"></i> <?php _trans('add_photo'); ?>
                </button>
                <!--
            <?php //else: ?>
                <button type="button" class="btn btn-default fileinput-button-photo form-control" disabled="disabled">
                    <?php //_trans('add_photo_after_save'); ?>
                </button>
                -->
            <?php //endif; ?>
        </div>

        <!-- dropzone -->
        <div class="row drop_zone">
            <input type="hidden" id="contact_photo" name="contact_photo" value="">
            <input type="hidden" id="temporary_contact_id" name="temporary_contact_id" value="<?php echo $contactId; ?>">
            <div id="actions" class="col-xs-12">
                <div id="previews_photo" class="table table-condensed files no-margin">
                    <div id="template_photo" class="file-row">
                        <!-- This is used as the file preview template -->
                        <div>
                            <span class="preview"><img data-dz-thumbnail/></span>
                        </div>
                        <div class="pull-right btn-group">
                            <?php if ($invoice->is_read_only != 1) { ?>
                                <button type="button" data-dz-remove class="btn btn-danger btn-sm delete btn-delete-photo">
                                    <i class="fa fa-trash-o"></i>
                                    <span><?php _trans('delete'); ?></span>
                                </button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- stop dropzone -->
    </div>

</div>
