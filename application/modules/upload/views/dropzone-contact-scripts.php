<script>
    var fileinputButtonPhotoElement;
    $('.fileinput-button-photo').click(function () {
        fileinputButtonPhotoElement = $(this);
    });

    var previewNodes = document.querySelectorAll('.contact #template_photo');
    var dropZones = document.querySelectorAll('.contact .drop_zone');
    var previewPhotos = document.querySelectorAll('.contact #previews_photo');
    var fileinputButtonPhotos = document.querySelectorAll('.contact .fileinput-button-photo');

    var previewTemplates = [];
    Array.prototype.forEach.call(previewNodes , function(previewNode) {
        previewNode.id = '';
        previewTemplates[previewTemplates.length] = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);
    });

    var myDropzones = [];
    previewTemplates.forEach(function(previewTemplate) {
        myDropzones[myDropzones.length] = new Dropzone(dropZones[myDropzones.length], { // Make the whole body a dropzone
            url: '<?php echo site_url('upload/upload_photo') ?>',
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            parallelUploads: 20,
            uploadMultiple: false,
            dictRemoveFileConfirmation: '<?php _trans('delete_photo_warning'); ?>',
            previewTemplate: previewTemplate,
            autoQueue: true, // Make sure the files aren't queued until manually added
            previewsContainer: previewPhotos[myDropzones.length], // Define the container to display the previews
            clickable: fileinputButtonPhotos[myDropzones.length], // Define the element that should be used as click trigger to select files.
            init: function () {
                var contactId = this.element.firstElementChild.nextElementSibling.getAttribute('value');
                var thisDropzone = this;
                $.ajax({
                    url: '<?php echo site_url('invoices/ajax/get_contact_photo'); ?>' + '?contact_id=' + contactId,
                    //url: '<?php //echo site_url('invoices/ajax/get_contact_photo/'); ?>' + contactId,
                    dataType: 'json',
                    type: 'get',
                    contentType: 'application/json',
                    async: false,
                    success: function(data) {
                        if (data.contact_photo && data.success) {
                            var mockFile = {};
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, '<?php echo site_url(); ?>' + data.contact_photo);
                            thisDropzone.emit('success', mockFile);

                            thisDropzone.element.firstElementChild.setAttribute('value', data.contact_photo);
                            thisDropzone.clickableElements[0].setAttribute('disabled', 'disabled');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        /*
                        var mockFile = {};
                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, '<?php //echo base_url('assets/core/img/file-icons/'); ?>' + 'default' + '.svg');
                        thisDropzone.emit('success', mockFile);
                        */
                    }
                });
                /*
                $.getJSON('<?php //echo site_url('invoices/ajax/get_contact_photo/') ?>' + contactId, function (data) {
                    console.log(data.contact_photo);
                });
                */
            }
        });
    });

    myDropzones.forEach(function(myDropzone) {
        myDropzone.on('success', function (file, response) {
            <?php echo(IP_DEBUG ? 'console.log(response);' : ''); ?>
            if (typeof response !== 'undefined') {
                response = JSON.parse(response);
                if (response.success === true) {
                    var parentNode = fileinputButtonPhotoElement;

                    do {
                        parentNode = parentNode.parent();
                    } while (parentNode[0].tagName != 'TBODY');
                    parentNode.find('#contact_photo').val(response.contact_photo);
                    parentNode.find('.fileinput-button-photo').attr('disabled','disabled');
                }
                if (response.success !== true) {
                    alert(response.message);
                }
            }
        });

        myDropzone.on('removedfile', function (file) {
            var contactPhoto = this.element.firstElementChild.getAttribute('value');
            this.element.firstElementChild.setAttribute('value', '');
            //var contactPhoto = file.previewElement.firstChild.nextSibling.getAttribute('value');
            this.clickableElements[0].removeAttribute('disabled');

            $.post({
                url: '<?php echo site_url('upload/delete_photo') ?>',
                data: {
                    'contact_photo': contactPhoto
                }
            });
        });
    });
</script>