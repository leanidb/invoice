<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2018 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Generate the PDF for an invoice
 *
 * @param $invoice_id
 * @param bool $stream
 * @param null $invoice_template
 * @param null $is_guest
 * @return string
 */
function generate_invoice_pdf($invoice_id, $stream = true, $invoice_template = null, $is_guest = null)
{
    $CI = &get_instance();

    $CI->load->model('invoices/mdl_items');
    $CI->load->model('invoices/mdl_invoices');
    $CI->load->model('invoices/mdl_invoice_tax_rates');
    $CI->load->model('custom_fields/mdl_custom_fields');
    $CI->load->model('payment_methods/mdl_payment_methods');

    $CI->load->helper('country');
    $CI->load->helper('client');

    $invoice = $CI->mdl_invoices->get_by_id($invoice_id);
    $invoice = $CI->mdl_invoices->get_payments($invoice);

    // Override language with system language
    set_language($invoice->client_language);

    if (!$invoice_template) {
        $CI->load->helper('template');
        $invoice_template = select_pdf_invoice_template($invoice);
    }

    $payment_method = $CI->mdl_payment_methods->where('payment_method_id', $invoice->payment_method)->get()->row();
    if ($invoice->payment_method == 0) {
        $payment_method = false;
    }

    // Determine if discounts should be displayed
    $items = $CI->mdl_items->where('invoice_id', $invoice_id)->get()->result();

    // Discount settings
    $show_item_discounts = false;
    foreach ($items as $item) {
        if ($item->item_discount != '0.00') {
            $show_item_discounts = true;
        }
    }

    // Get all custom fields
    $custom_fields = array(
        'invoice' => $CI->mdl_custom_fields->get_values_for_fields('mdl_invoice_custom', $invoice->invoice_id),
        'client' => $CI->mdl_custom_fields->get_values_for_fields('mdl_client_custom', $invoice->client_id),
        'user' => $CI->mdl_custom_fields->get_values_for_fields('mdl_user_custom', $invoice->user_id),
    );

    if ($invoice->quote_id) {
        $custom_fields['quote'] = $CI->mdl_custom_fields->get_values_for_fields('mdl_quote_custom', $invoice->quote_id);
    }

    // PDF associated files
    $include_zugferd = $CI->mdl_settings->setting('include_zugferd');

    if ($include_zugferd) {
        $CI->load->helper('zugferd');

        $associatedFiles = array(
            array(
                'name' => 'ZUGFeRD-invoice.xml',
                'description' => 'ZUGFeRD Invoice',
                'AFRelationship' => 'Alternative',
                'mime' => 'text/xml',
                'path' => generate_invoice_zugferd_xml_temp_file($invoice, $items)
            )
        );
    } else {
        $associatedFiles = null;
    }

    $CI->load->model('invoices/mdl_meetings');
    $invoiceMeetings =  $CI->mdl_meetings->where('invoice_id', $invoice_id)->order_by('meeting_order')->get()->result();

    $CI->load->model('invoices/mdl_invoice_contacts');
    $invoiceContacts = $CI->mdl_invoice_contacts->get_join_table()->where('invoice_id', $invoice_id)->order_by('contact_order')->get()->result();

    $data = array(
        'invoice' => $invoice,
        'invoice_tax_rates' => $CI->mdl_invoice_tax_rates->where('invoice_id', $invoice_id)->get()->result(),
        'items' => $items,
        'payment_method' => $payment_method,
        'output_type' => 'pdf',
        'show_item_discounts' => $show_item_discounts,
        'custom_fields' => $custom_fields,
        'invoice_meetings' => $invoiceMeetings,
        'invoice_contacts' => $invoiceContacts,
    );

    $html = $CI->load->view('invoice_templates/pdf/' . $invoice_template, $data, true);

    $CI->load->helper('mpdf');
    return pdf_create($html, trans('invoice') . '_' . str_replace(array('\\', '/'), '_', $invoice->invoice_number),
        $stream, $invoice->invoice_password, true, $is_guest, $include_zugferd, $associatedFiles);
}

function generate_invoice_sumex($invoice_id, $stream = true, $client = false)
{
    $CI = &get_instance();

    $CI->load->model('invoices/mdl_items');
    $invoice = $CI->mdl_invoices->get_by_id($invoice_id);
    $CI->load->library('Sumex', array(
        'invoice' => $invoice,
        'items' => $CI->mdl_items->where('invoice_id', $invoice_id)->get()->result()
    ));

    // Append a copy at the end and change the title:
    // WARNING: The title depends on what invoice type is (TP, TG)
    // and is language-dependant. Fix accordingly if you really need this hack
    $temp = tempnam("/tmp", "invsumex_");
    $tempCopy = tempnam("/tmp", "invsumex_");
    $pdf = new FPDI();
    $sumexPDF = $CI->sumex->pdf();

    $sha1sum = sha1($sumexPDF);
    $shortsum = substr($sha1sum, 0, 8);
    $filename = trans('invoice') . '_' . $invoice->invoice_number . '_' . $shortsum;

    if (!$client) {
        file_put_contents($temp, $sumexPDF);

        // Hackish
        $sumexPDF = str_replace(
            "Giustificativo per la richiesta di rimborso",
            "Copia: Giustificativo per la richiesta di rimborso",
            $sumexPDF
        );

        file_put_contents($tempCopy, $sumexPDF);

        $pageCount = $pdf->setSourceFile($temp);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $size = $pdf->getTemplateSize($templateId);

            if ($size['w'] > $size['h']) {
                $pageFormat = 'L';  //  landscape
            } else {
                $pageFormat = 'P';  //  portrait
            }

            $pdf->addPage($pageFormat, array($size['w'], $size['h']));
            $pdf->useTemplate($templateId);
        }

        $pageCount = $pdf->setSourceFile($tempCopy);

        for ($pageNo = 2; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $size = $pdf->getTemplateSize($templateId);

            if ($size['w'] > $size['h']) {
                $pageFormat = 'L';  //  landscape
            } else {
                $pageFormat = 'P';  //  portrait
            }

            $pdf->addPage($pageFormat, array($size['w'], $size['h']));
            $pdf->useTemplate($templateId);
        }

        unlink($temp);
        unlink($tempCopy);

        if ($stream) {
            header("Content-Type", "application/pdf");
            $pdf->Output($filename . '.pdf', 'I');
            return;
        }

        $filePath = UPLOADS_TEMP_FOLDER . $filename . '.pdf';
        $pdf->Output($filePath, 'F');
        return $filePath;
    } else {
        if ($stream) {
            return $sumexPDF;
        }

        $filePath = UPLOADS_TEMP_FOLDER . $filename . '.pdf';
        file_put_contents($filePath, $sumexPDF);
        return $filePath;
    }
}

/**
 * Generate the PDF for a quote
 *
 * @param $quote_id
 * @param bool $stream
 * @param null $quote_template
 *
 * @return string
 * @throws \Mpdf\MpdfException
 */
function generate_quote_pdf($quote_id, $stream = true, $quote_template = null)
{
    $CI = &get_instance();

    $CI->load->model('quotes/mdl_quotes');
    $CI->load->model('quotes/mdl_quote_items');
    $CI->load->model('quotes/mdl_quote_tax_rates');
    $CI->load->model('custom_fields/mdl_custom_fields');
    $CI->load->helper('country');
    $CI->load->helper('client');

    $quote = $CI->mdl_quotes->get_by_id($quote_id);

    // Override language with system language
    set_language($quote->client_language);

    if (!$quote_template) {
        $quote_template = $CI->mdl_settings->setting('pdf_quote_template');
    }

    // Determine if discounts should be displayed
    $items = $CI->mdl_quote_items->where('quote_id', $quote_id)->get()->result();

    $show_item_discounts = false;
    foreach ($items as $item) {
        if ($item->item_discount != '0.00') {
            $show_item_discounts = true;
        }
    }

    // Get all custom fields
    $custom_fields = array(
        'quote' => $CI->mdl_custom_fields->get_values_for_fields('mdl_quote_custom', $quote->quote_id),
        'client' => $CI->mdl_custom_fields->get_values_for_fields('mdl_client_custom', $quote->client_id),
        'user' => $CI->mdl_custom_fields->get_values_for_fields('mdl_user_custom', $quote->user_id),
    );

    $data = array(
        'quote' => $quote,
        'quote_tax_rates' => $CI->mdl_quote_tax_rates->where('quote_id', $quote_id)->get()->result(),
        'items' => $items,
        'output_type' => 'pdf',
        'show_item_discounts' => $show_item_discounts,
        'custom_fields' => $custom_fields,
    );

    $html = $CI->load->view('quote_templates/pdf/' . $quote_template, $data, true);

    $CI->load->helper('mpdf');

    return pdf_create($html, trans('quote') . '_' . str_replace(array('\\', '/'), '_', $quote->quote_number), $stream, $quote->quote_password);
}

function create_diagram($invoice)
{
    $percent = round(100 * $invoice->hours_completed / $invoice->hours_total);
    $diagramFile = FCPATH . 'uploads/customer_files/' . $invoice->invoice_id . '.jpg';

    $dst_w = 320;
    $dst_h = 320;
    $thickness = 6;
    $angle = ($percent <= 50) ? -180 + 360 * $percent / 100 : 360 * ($percent - 50) / 100;

    $dst_img = imagecreatetruecolor($dst_w, $dst_h);
    $greyColor = imagecolorallocate($dst_img, 224, 224, 224);
    imagefilledellipse($dst_img, $dst_w / 2, $dst_h / 2, $dst_w, $dst_h, $greyColor);

    $whiteColor = imagecolorallocate($dst_img, 255, 255, 255);
    imagefilledellipse($dst_img, $dst_w / 2, $dst_h / 2, $dst_w - $thickness, $dst_h - $thickness, $whiteColor);

    $blueColor = imagecolorallocate($dst_img, 0, 0, 255);

    for ($i=0; $i<=$thickness; $i++) {
        imagefilledarc($dst_img, $dst_w / 2, $dst_h / 2, $dst_w - $i, $dst_h - $i, -180, $angle, $blueColor, IMG_ARC_NOFILL);
    }

    $font = FCPATH . 'fonts/GothamProRegular.ttf';
    $textColor = imagecolorallocate($dst_img, 0, 0, 0);
    imagettftext($dst_img, 50, 0, 100, 160, $textColor, $font, $percent . '%');
    imagettftext($dst_img, 22, 0, 80, 210, $textColor, $font, 'Completed');

    // Fill each corners of destination image with transparency
    imagefill($dst_img, 0, 0, $whiteColor);
    imagefill($dst_img, $dst_w - 1, 0, $whiteColor);
    imagefill($dst_img, 0, $dst_h - 1, $whiteColor);
    imagefill($dst_img, $dst_w - 1, $dst_h - 1, $whiteColor);
    imagecolortransparent($dst_img, $whiteColor);

    imagejpeg($dst_img, $diagramFile, 100);

    return $diagramFile;
}

function formatted_hours($hours)
{
    $seconds = 3600 * $hours;
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    $days = $dtF->diff($dtT)->format('%d');
    for ($weeks=0; $days>=7; $days=$days-7) {
        $weeks++;
    }
    $timeString = $dtF->diff($dtT)->format('%d days and %h hours');
    $formattedHours = $weeks . ' weeks ' . $days . ' ' . substr($timeString, strrpos($timeString, 'days'));

    return $formattedHours;
}

function create_circle($imgPath)
{
    /*
    if (stristr($imgPath, '.bmp') !== false) {
        $img = imagecreatefrombmp($imgPath);
    } elseif (stristr($imgPath, '.png') !== false) {
        $img = imagecreatefrompng($imgPath);
    } elseif (stristr($imgPath, '.jpeg') !== false || stristr($imgPath, '.jpg') !== false) {
        $img = imagecreatefromjpeg($imgPath);
    }
    $iconFile = str_replace('.bmp', '_icon.bmp', str_replace('.png', '_icon.png', str_replace('.jpeg', '_icon.jpeg', str_replace('.jpg', '_icon.jpg', $imgPath))));
    */

    if (stristr($imgPath, '.png') !== false) {
        $img = imagecreatefrompng($imgPath);
    } elseif (stristr($imgPath, '.jpeg') !== false || stristr($imgPath, '.jpg') !== false) {
        $img = imagecreatefromjpeg($imgPath);
    }
    $iconFile = str_replace('.png', '_icon.png', str_replace('.jpeg', '_icon.jpeg', str_replace('.jpg', '_icon.jpg', $imgPath)));

    $src_img = $img;
    $src_w = imagesx($img);
    $src_h = imagesy($img);
    $dst_w = 2*imagesx($img);
    $dst_h = 2*imagesy($img);

    $dst_img = imagecreatetruecolor($dst_w, $dst_h);
    //imagecopy($dst_img, $src_img, 0, 0, 0, 0, $dst_w, $dst_h);
    imagecopyresized($dst_img, $src_img, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h);

    // Create a black image with a transparent ellipse, and merge with destination
    $mask = imagecreatetruecolor($dst_w, $dst_h);
    $maskTransparent = imagecolorallocate($mask, 255, 0, 255);
    imagecolortransparent($mask, $maskTransparent);
    imagefilledellipse($mask, $dst_w / 2, $dst_h / 2, $dst_w, $dst_h, $maskTransparent);
    imagecopymerge($dst_img, $mask, 0, 0, 0, 0, $dst_w, $dst_h, 100);

    // Fill each corners of destination image with transparency
    $dstTransparent = imagecolorallocate($dst_img, 255, 255, 255);
    imagefill($dst_img, 0, 0, $dstTransparent);
    imagefill($dst_img, $dst_w - 1, 0, $dstTransparent);
    imagefill($dst_img, 0, $dst_h - 1, $dstTransparent);
    imagefill($dst_img, $dst_w - 1, $dst_h - 1, $dstTransparent);
    imagecolortransparent($dst_img, $dstTransparent);

    imagejpeg($dst_img, $iconFile, 100);

    return $iconFile;
}
/*
function imagecreatefrombmp($fileName) {
    $file = fopen($fileName, "rb");
    $read = fread($file, 10);
    while (!feof($file) && ($read <> ""))
        $read .= fread($file, 1024);
    $temp = unpack("H*", $read);
    $hex = $temp[1];
    $header = substr($hex, 0, 108);
    if (substr($header, 0, 4) == "424d") {
        $parts = str_split($header, 2);
        $width = hexdec($parts[19] . $parts[18]);
        $height = hexdec($parts[23] . $parts[22]);
        unset($parts);
    }
    $x = 0;
    $y = 1;
    $image = imagecreatetruecolor($width, $height);
    $body = substr($hex, 108);
    $body_size = (strlen($body) / 2);
    $header_size = ($width * $height);
    $usePadding = ($body_size > ($header_size * 3) + 4);
    for ($i = 0; $i < $body_size; $i+=3) {
        if ($x >= $width) {
            if ($usePadding)
                $i += $width % 4;
            $x = 0;
            $y++;
            if ($y > $height)
                break;
        }
        $i_pos = $i * 2;
        $r = hexdec($body[$i_pos + 4] . $body[$i_pos + 5]);
        $g = hexdec($body[$i_pos + 2] . $body[$i_pos + 3]);
        $b = hexdec($body[$i_pos] . $body[$i_pos + 1]);
        $color = imagecolorallocate($image, $r, $g, $b);
        imagesetpixel($image, $x, $height - $y, $color);
        $x++;
    }
    fclose($file);
    unset($body);
    return $image;
}
*/
